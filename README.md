## NODE VERSION
- v13.10.1

## CHALLENGES
- Time consuming to grab all the data from Palpatine's api. Ideally I'd be storing any completed decryptions locally in a database. Then only need to hit the api once per cipher. 
Subsequent requests can just grab locally cached data.
- I just went with a liberal 60s per 4 concurrent executions w/ each execution a body of 1000 entries. Made sure I'm safe from being throttled by the api. No doubt parameters for
the network requests could be tuned for more optimal decryption.
