const fs = require('fs');
const _ = require('lodash');
const { mapSeries } = require('async');
const { decrypt, getHomeWorld } = require('./lib/api');
const { formatGroupOutput } = require('./lib/writer');

const CHUNK_SIZE = 1000;
const CONCURRENT_PALPATINE_EXECUTIONS = 4;
const DELAY = 60000;

(async () => {
  const data = fs.readFileSync('./data/super-secret-data.txt', 'utf8');

  // SEPARATE DATA INTO GROUPS OF WHAT WILL BE ASYNC EXECUTIONS INTO GROUPS OF 1000 DATA POINTS
  const chunkList = _.chunk(_.chunk(_.uniq(data.split('\n').filter((item) => item !== '')), CHUNK_SIZE), CONCURRENT_PALPATINE_EXECUTIONS);

  // SYNCHRONOUS EXECUTION OF ASYNC CONCURRENT_PALPATINE_EXECUTIONS
  const decryptedEntries = _.compact(_.flattenDeep(await mapSeries(chunkList, (itemBundle, cb) => {
    setTimeout(async () => {
      const dItems = await Promise.all(itemBundle.map((item) => decrypt(item)));
      cb(null, dItems);
    }, DELAY);
  })));

  // GRAB HOMEWORLDS FROM THE STAR WARS API. GROUPED BY HOMEWORLD.
  const homeWorlds = _.groupBy(await Promise.all(_.uniqBy(decryptedEntries, (item) => item.name)
    .map(async (entry) => {
      const homeWorld = await getHomeWorld(entry.homeworld);
      return {
        name: entry.name,
        homeWorld,
      };
    })), (item) => item.homeWorld);

  // WRITE TO FILE, IN SORTED ORDER
  fs.writeFileSync('./data/citizens-super-secret-info.txt', _.orderBy(_.values(homeWorlds), [(item) => item[0].homeWorld.toLowerCase()])
    .reduce((accum, item) => accum + formatGroupOutput(item), ''));
})();
