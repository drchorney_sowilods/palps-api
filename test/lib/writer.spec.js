const { expect } = require('chai');
const { formatGroupOutput } = require('../../lib/writer.js');

const testData = [
  { name: 'C-3PO', homeWorld: 'Tatooine' },
  { name: 'Darth Vader', homeWorld: 'Tatooine' },
];

describe('The writer.js lib file', () => {
  it('should output a properly formatted home world group', () => {
    const output = formatGroupOutput(testData);
    expect(output).to.equal('Tatooine\n- C-3PO\n- Darth Vader\n\n');
  });
});
