const config = require('config');
const axios = require('axios');

const palpsApi = axios.create({
  baseURL: config.get('baseUrl'),
  timeout: config.get('timeout'),
  headers: { 'x-api-key': process.env.PALP_TOKEN },
});

const decrypt = (data) => palpsApi.post('/prod/decrypt', data).then((res) => res.data.map(JSON.parse))
  .catch(console.log);

const getHomeWorld = (url) => axios.get(url).then((res) => res.data.name)
  .catch(console.log);

module.exports = {
  decrypt,
  getHomeWorld,
};
