const formatGroupOutput = (worldDataGroup) => {
  const homeworld = `${worldDataGroup[0].homeWorld}\n`;
  /* eslint-disable-next-line prefer-template */
  return worldDataGroup.reduce((accum, item) => accum + `- ${item.name}\n`, homeworld) + '\n';
};

module.exports = {
  formatGroupOutput,
};
